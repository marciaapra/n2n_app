import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

buildDateField(bool dateOnly, DateFormat format, bool editable, String label,
    Function onChange, DateTime value,
    [Function validator]) {
  return DateTimePickerFormField(
      initialValue: value,
      dateOnly: dateOnly,
      format: format,
      editable: editable,
      decoration: InputDecoration(
          labelText: label, labelStyle: TextStyle(color: primaryColor)),
      style: TextStyle(color: primaryColor),
      onChanged: onChange,
      validator: validator);
}
