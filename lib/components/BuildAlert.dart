import 'package:flutter/material.dart';

Widget buildAlert(Text titulo, String text, BuildContext context) {
  return AlertDialog(
    title: titulo,
    content: Text(text),
    actions: <Widget>[
      FlatButton(
        child: Text('Ok'),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    ],
  );
}

Widget buildAlertField(Widget content, BuildContext context) {
  return AlertDialog(
    title: Text('Para continuar informe:'),
    content: content,
    actions: <Widget>[
      FlatButton(
        child: Text('Cancelar'),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      FlatButton(
        child: Text('Alterar Dados'),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    ],
  );
}
