import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';

Widget buildButton(String text, Function onPressed) {
  return RaisedButton(
    child: Text(
      text,
      style: TextStyle(fontSize: 18.0),
    ),
    textColor: Colors.white,
    color: secondaryColor,
    padding: EdgeInsets.all(10.0),
    onPressed: onPressed,
  );
}
