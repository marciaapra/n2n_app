import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';

buildTextField(String label, secret, type, controller,
    [Function validate, Function pressIcon]) {
  var icone;
  if (pressIcon != null) {
    icone = IconButton(
        icon: Icon(
          secret ? Icons.visibility_off : Icons.visibility,
        ),
        onPressed: pressIcon);
  }

  return TextFormField(
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: primaryColor),
        suffixIcon: icone,
      ),
      keyboardType: type,
      obscureText: secret,
      style: TextStyle(color: primaryColor),
      validator: validate,
      controller: controller);
}

buildTextField2(param) {
  var icone;
  if (param['pressIcon'] != null) {
    icone = IconButton(
        icon: Icon(
          param['secret'] ? Icons.visibility_off : Icons.visibility,
        ),
        onPressed: param['pressIcon']);
  }

  return TextFormField(
      decoration: InputDecoration(
        labelText: param['label'],
        labelStyle: TextStyle(color: primaryColor),
        suffixIcon: icone,
      ),
      keyboardType: param['type'],
      obscureText: param['secret'],
      style: TextStyle(color: primaryColor),
      initialValue: param['initialValue'],
      validator: param['validate'],
      onSaved: param['onSaved']);
}
