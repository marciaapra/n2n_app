import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';

buildDropDown(String label, List opcoes, value, Function onChange,
    [Function validator]) {
  return DropdownButtonFormField(
      decoration: InputDecoration(
          labelText: label, labelStyle: TextStyle(color: primaryColor)),
      items: opcoes,
      value: value,
      onChanged: onChange,
      validator: validator);
}
