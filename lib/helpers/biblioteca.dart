import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:http/http.dart' as http;

//Lista de opções de gêneros
List<DropdownMenuItem<String>> optionsGeneros() {
  List<DropdownMenuItem<String>> items = new List();
  items.add(new DropdownMenuItem(value: 'F', child: new Text('Feminino')));
  items.add(new DropdownMenuItem(value: 'M', child: new Text('Masculino')));
  items.add(
      new DropdownMenuItem(value: 'D', child: new Text('Desejo não informar')));
  return items;
}

//Lista de opções de telefones
List<DropdownMenuItem<String>> optionsTelefones() {
  List<DropdownMenuItem<String>> items = new List();
  items.add(new DropdownMenuItem(value: 'CEL', child: new Text('Celular')));
  items.add(new DropdownMenuItem(value: 'COM', child: new Text('Comercial')));
  items.add(new DropdownMenuItem(value: 'RES', child: new Text('Residencial')));
  return items;
}

//Validador de e-mail
bool validadorEmail(value) {
  //Expressão para e-mail
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  //Cria um regex com a pattern do e-mail
  RegExp regex = new RegExp(pattern);

  //Verifica se o e-mail não é correspondente
  if (!regex.hasMatch(value))
    return false; //E-mail inválido
  else
    return true; //E-mail válido
}

//Converte senha para sha256
Digest converterSenha(String senha) {
  var senhaUtf = utf8.encode(senha);
  var senhaConv = sha256.convert(senhaUtf);

  return senhaConv;
}

//Valida se a senha possui uma letra maiúscula, uma minúscula, um número, um caracter especial e que seja entre 8 e 16 caracteres
bool validarSenha(String senha) {
  RegExp exp =
      new RegExp(r"^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,16}$");
  return exp.hasMatch(senha);
}

//Remove a palavra Exception da mensagem de erro
String tratarMsgErro(String msg) {
  return msg.replaceAll('Exception:', '');
}

Future<Map> consultarCep(cep) async {
  final url = apiCep + cep + '/json';

  return http.get(url).then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}

//Trata qualquer json removendo atributos nulos
removerDadosNulos(dados) {
  Map<String, dynamic> preenchidos = {};
  for (String key in dados.keys) {
    if (dados[key].runtimeType == String || dados[key].runtimeType == Null) {
      if (dados[key] != null && dados[key] != "") {
        preenchidos.addAll({key: dados[key]});
      }
    } else {
      var retorno = removerDadosNulos(dados[key]);
      if (retorno.toString() != '{}') {
        preenchidos.addAll({key: retorno});
      }
    }
  }
  return preenchidos;
}

//Trata qualquer json removendo atributos não nulos
apenasNulos(dados) {
  Map<String, dynamic> nulos = {};
  for (String key in dados.keys) {
    if (dados[key].runtimeType == String || dados[key].runtimeType == Null) {
      if (dados[key] == null || dados[key] == "") {
        nulos.addAll({key: null});
      }
    } else {
      var retorno = apenasNulos(dados[key]);
      if (retorno.toString() != '{}') {
        nulos.addAll({key: retorno});
      }
    }
  }
  return nulos;
}

//Trata o nome do campo a ser exibido
tratarCampo(campo) {
  switch (campo) {
    case 'email':
      return "E-mail";
      break;
    case "cpf":
      return "CPF";
      break;
    case "nome_completo":
      return "Nome Completo";
      break;
    case "apelido":
      return "Apelido";
      break;
    case "data_nasc":
      return "Data de Nascimento";
      break;
    case "genero":
      return "Gênero";
      break;
    case "nacionalidade":
      return "Nacionalidade";
      break;
    default:
      return campo;
      break;
  }
}
