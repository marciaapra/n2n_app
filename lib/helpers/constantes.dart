import 'package:flutter/material.dart';

/* CORES */
const primaryColor = Color.fromARGB(255, 42, 58, 75); //Azul escuro
const secondaryColor = Color.fromARGB(255, 25, 176, 145); //Verde claro

/* ENDPOINTS */
const apiUrl = 'https://n-server-v2.herokuapp.com/';
const apiCep = 'viacep.com.br/ws/';
