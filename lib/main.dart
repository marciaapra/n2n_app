import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/screens/cadastro.dart';
import 'package:n2n_app/screens/home.dart';
import 'package:n2n_app/screens/login.dart';
import 'package:n2n_app/services/authService.dart';

AuthService appAuth = new AuthService();

void main() async {
  Widget _defaultHome = new Login();

  bool _result = await appAuth.isLogado();
  if (_result) {
    _defaultHome = new Home();
  }

  runApp(MaterialApp(
    title: 'N2N',
    home: _defaultHome,
    routes: <String, WidgetBuilder>{
      '/login': (BuildContext context) => new Login(),
      '/cadastro': (BuildContext context) => new Cadastro(),
      '/home': (BuildContext context) => new Home(),
    },
    theme: ThemeData(
      primarySwatch: Colors.blue,
      primaryColor: primaryColor,
      accentColor: secondaryColor,
      bottomAppBarColor: primaryColor,
    ),
    localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate
    ],
    debugShowCheckedModeBanner: false,
    supportedLocales: [
      Locale('pt'),
    ],
  ));
}
