import 'package:flutter/material.dart';
import 'package:n2n_app/components/BuildAlert.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/components/BuildLoading.dart';
import 'package:n2n_app/components/BuiltTextField.dart';
import 'package:crypto/crypto.dart';
import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/services/loginService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final loginController = TextEditingController();
  final senhaController = TextEditingController();
  bool loading = false;
  bool ocultarSenha = true;

  //Verifica se exibe ou oculta senha
  void modoSenha() {
    setState(() {
      ocultarSenha = !ocultarSenha;
    });
  }

  login() {
    //Seta variavel de loading true
    setState(() {
      loading = true;
    });

    //Criptografa senha
    Digest senha = converterSenha(senhaController.text);

    //Verifica se o login é por CPF ou e-mail
    var tipoLogin = CPFValidator.isValid(loginController.text) ? "0" : "1";

    //Trata dados para envio de cadastro
    var dados = {
      'metodo_entrada': loginController.text.toString(),
      'senha': senha.toString(),
      'tipo_entrada': tipoLogin
    };

    //Chama método cadastrar pessoa
    logar(dados).then((response) async {
      //Grava dados em uma seção
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('segredo', response['objeto']['segredo']);
      prefs.setString('nome', response['objeto']['nome_usuario']);

      //Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Redireciona para Home
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    }).catchError((error) {
      //Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Exibe alerta de erro
      return showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return buildAlert(
                Text('ATENÇÃO'), tratarMsgErro(error.toString()), context);
          });
    });
  }

  //Conteúdo da página
  Widget page() {
    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.all(16.0),
        children: <Widget>[
          Padding(padding: EdgeInsets.all(16.0)),
          Hero(
            tag: 'hero',
            child: CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 48.0,
              child: Image.asset('images/logo.png'),
            ),
          ),
          buildTextField('E-mail ou CPF', false, TextInputType.emailAddress,
              loginController, (text) {
            if (text.isEmpty) {
              return "E-mail ou CPF precisa ser preenchido!";
            } else if (!CPFValidator.isValid(text) && !validadorEmail(text)) {
              return "Formato de E-mail ou CPF inválido!";
            }
            return null;
          }),
          SizedBox(
            height: 16.0,
          ),
          buildTextField(
              "Senha", ocultarSenha, TextInputType.text, senhaController,
              (text) {
            if (text.isEmpty) {
              return "Senha precisa ser preenchida!";
            }
            return null;
          }, () {
            modoSenha();
          }),
          SizedBox(
            height: 16.0,
          ),
          buildButton("Entrar", () {
            if (_formKey.currentState.validate()) {
              login();
            }
          }),
          SizedBox(
            height: 50.0,
            child: Align(
              alignment: Alignment.center,
              child: FlatButton(
                child: Text(
                  "Não tem conta? Cadastre-se",
                  style: TextStyle(color: Colors.grey),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed('/cadastro');
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  //Verifica status de loading para exibir a tela de carregamento ou o conteúdo da página
  Widget conteudo() {
    if (loading) {
      return loadingView();
    } else {
      return page();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(''),
          centerTitle: true,
        ),
        body: conteudo());
  }
}
