import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:n2n_app/components/BuildAlert.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/models/projeto.dart';
import 'package:n2n_app/screens/historicoConsulta.dart';
import 'package:n2n_app/screens/home.dart';
import 'package:n2n_app/services/projetoService.dart';

class DetalhePermissao extends StatefulWidget {
  final Projeto projeto;
  DetalhePermissao({Key key, this.projeto}) : super(key: key);

  @override
  _DetalhePermissaoState createState() => _DetalhePermissaoState();
}

class _DetalhePermissaoState extends State<DetalhePermissao> {
  List<ExpansionTile> _expansionTile() {
    return new List.generate(widget.projeto.requerimentos.length, (int index) {
      //widget.projeto.requerimentos[index].campo
      return new ExpansionTile(
          title: Text(tratarCampo(widget.projeto.requerimentos[index].campo)),
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text(widget.projeto.requerimentos[index].motivo))
          ]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar(
          'Detalhes',
        ),
        body: ListView(padding: EdgeInsets.symmetric(vertical: 8.0), children: <
            Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Card(
                        elevation: 8.0,
                        margin: EdgeInsets.symmetric(
                            horizontal: 3.0, vertical: 6.0),
                        child: Container(
                          padding: EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                '${widget.projeto.nome}',
                                style: TextStyle(
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                                textAlign: TextAlign.center,
                              ),
                              Container(padding: EdgeInsets.all(10.0)),
                              Text(
                                widget.projeto.aprovado
                                    ? 'Acesso aos dados autorizado'
                                    : 'Acesso aos dados não autorizado',
                                style: TextStyle(
                                    color: widget.projeto.aprovado
                                        ? Colors.green
                                        : Colors.red),
                              ),
                              Container(padding: EdgeInsets.all(10.0)),
                              Text(
                                  'Aprovação em ' +
                                      formatDate(widget.projeto.dataAprovacao, [
                                        dd,
                                        '/',
                                        mm,
                                        '/',
                                        yyyy,
                                        ' ',
                                        HH,
                                        ':',
                                        nn
                                      ]),
                                  textAlign: TextAlign.start),
                              Container(padding: EdgeInsets.all(3.0)),
                              Text(
                                  'Atualizado em ' +
                                      formatDate(
                                          widget.projeto.dataAtualizacao, [
                                        dd,
                                        '/',
                                        mm,
                                        '/',
                                        yyyy,
                                        ' ',
                                        HH,
                                        ':',
                                        nn
                                      ]),
                                  textAlign: TextAlign.start),
                            ],
                          ),
                        ))),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Card(
                    elevation: 8.0,
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(15.0),
                          child: Text('Informações Autorizadas para uso:',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0)),
                        ),
                        Column(children: _expansionTile()),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: buildButton(
                      widget.projeto.aprovado
                          ? 'Recusar Permissão de Acesso'
                          : 'Aceitar Permissão de Acesso', () {
                    aceitarTermos(widget.projeto.id, !widget.projeto.aprovado)
                        .then((response) async {
                      Navigator.pop(context, true);
                      return showDialog<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return buildAlert(Text('SUCESSO'),
                                "As permissões foram atualizadas.", context);
                          });
                    }).catchError((error) {
                      //Exibe alerta de erro
                      return showDialog<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return buildAlert(Text('ATENÇÃO'),
                                tratarMsgErro(error.toString()), context);
                          });
                    });
                  }),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: buildButton("Histórico de Consultas", () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                HistoricoConsulta(projeto: widget.projeto)));
                  }),
                )
              ])
        ]));
  }
}
