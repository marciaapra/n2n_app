import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';

class Dashboard extends StatelessWidget {

  final int id;
  final String titulo;
  final String icon;
  final String texto;
  final bool horizontal;

  Dashboard(this.id,this.icon,this.titulo,this.texto, {this.horizontal = true});

  Dashboard.vertical(this.id,this.icon,this.titulo,this.texto): horizontal = false;

  @override
  Widget build(BuildContext context) {

    final dashboardThumbnail = new Container(
      margin: new EdgeInsets.symmetric(
        vertical: 20.0
      ),
      alignment: horizontal ? FractionalOffset.centerLeft : FractionalOffset.center,
      child: new Hero(
          tag: "card-${this.id}",
          child: new Image(
          image: new AssetImage(icon),
          height: 60.0,
          width: 60.0,
        ),
      ),
    );

    final dashboardCardContent =Container(
      margin:EdgeInsets.fromLTRB(horizontal ? 45.0 : 16.0, horizontal ? 16.0 : 42.0, 16.0, 16.0),
      constraints:BoxConstraints.expand(),
      child:Column(
        crossAxisAlignment: horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        children: <Widget>[
          Container(height: 4.0),
          Text(this.titulo, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16.0)),
          Container(height: 10.0),
          Text(this.texto,style: TextStyle(color: Colors.grey[200]))
        ],
      ),
    );

    final dashboardCard = new Container(
      child: dashboardCardContent,
      height: horizontal ? 100.0 : 154.0,
      margin: horizontal
        ? new EdgeInsets.only(left: 25.0)
        : new EdgeInsets.only(top: 72.0),
      decoration: new BoxDecoration(
        color: secondaryColor,
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: new Offset(0.0, 10.0),
          ),
        ],
      ),
    );


    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: 24.0,
        ),
        child: new Stack(
          children: <Widget>[
            dashboardCard,
            dashboardThumbnail,
          ],
        ),
    );
  }
}