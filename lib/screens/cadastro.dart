import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:n2n_app/components/BuildAlert.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/components/BuildLoading.dart';
import 'package:n2n_app/components/BuiltDateField.dart';
import 'package:n2n_app/components/BuiltDropDown.dart';
import 'package:n2n_app/components/BuiltTextField.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/services/pessoaService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Cadastro extends StatefulWidget {
  @override
  _CadastroState createState() => _CadastroState();
}

class _CadastroState extends State<Cadastro> {
  final _formKey = GlobalKey<FormState>();

  final nomeController = TextEditingController();
  final cpfController = TextEditingController();
  final rgController = TextEditingController();
  final emRgController = TextEditingController();
  final emailController = TextEditingController();
  final senhaController = TextEditingController();
  final confSenhaController = TextEditingController();

  DateTime _date;
  String _genero;
  bool loading = false;
  bool ocultarSenha = true;
  bool ocultarConfSenha = true;

  //Verifica se exibe ou oculta senha
  void modoSenha(campo) {
    if (campo == 'senha') {
      setState(() {
        ocultarSenha = !ocultarSenha;
      });
    } else {
      setState(() {
        ocultarConfSenha = !ocultarConfSenha;
      });
    }
  }

  cadastrar() {
    //Seta variavel de loading true
    setState(() {
      loading = true;
    });

    //Criptografa senha
    Digest senha = converterSenha(senhaController.text);

    //Trata dados para envio de cadastro
    var dados = {
      'nome_completo': nomeController.text,
      'cpf': cpfController.text.toString(),
      'data_nasc': _date.toString(),
      'genero': _genero,
      'email': emailController.text,
      'senha': senha.toString(),
    };

    //Chama método cadastrar pessoa
    cadastrarPessoa(dados).then((response) async {
      //Grava dados em uma seção
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('segredo', response['objeto']['segredo']);
      prefs.setString('nome', response['objeto']['nome_usuario']);

      //Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Redireciona para Home
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    }).catchError((error) {
      //Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Exibe alerta de erro
      return showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return buildAlert(
                Text('ATENÇÃO'), tratarMsgErro(error.toString()), context);
          });
    });
  }

  Widget page() {
    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.all(16.0),
        children: <Widget>[
          buildTextField(
              'Nome Completo', false, TextInputType.text, nomeController,
              (text) {
            if (text.isEmpty) {
              return "Nome Completo é obrigatório!";
            }
            return null;
          }),
          SizedBox(
            height: 10.0,
          ),
          buildTextField('CPF', false, TextInputType.number, cpfController,
              (text) {
            if (text.isEmpty) {
              return "CPF é obrigatório!";
            } else if (!CPFValidator.isValid(text)) {
              return "Formato de CPF inválido!";
            }
            return null;
          }),
          SizedBox(
            height: 10.0,
          ),
          buildDateField(
              true,
              DateFormat('dd/MM/yyyy'),
              true,
              'Data de Nascimento',
              (value) => setState(() => _date = value),
              null, (text) {
            if (text == null) {
              return "Data de Nascimento é obrigatório!";
            }
            return null;
          }),
          SizedBox(
            height: 10.0,
          ),
          buildDropDown('Gênero', optionsGeneros(), _genero,
              (value) => setState(() => _genero = value), (text) {
            if (text == null) {
              return "Gênero é obrigatório!";
            }
            return null;
          }),
          SizedBox(
            height: 10.0,
          ),
          buildTextField(
              'E-mail', false, TextInputType.emailAddress, emailController,
              (text) {
            if (text.isEmpty) {
              return "E-mail é obrigatório!";
            } else if (!validadorEmail(text)) {
              return "Formato de e-mail inválido!";
            }
            return null;
          }),
          SizedBox(
            height: 10.0,
          ),
          buildTextField(
              'Senha', ocultarSenha, TextInputType.text, senhaController,
              (text) {
            if (text.isEmpty) {
              return "Senha é obrigatório!";
            } else if (text.length < 8 && text.length > 16) {
              return "Senha deve ter entre 8 a 16 caracteres.";
            } else if (!validarSenha(text)) {
              return "Senha deve conter maiúscula, minúscula, caracter especial\ne número.";
            }
            return null;
          }, () {
            modoSenha('senha');
          }),
          SizedBox(
            height: 16.0,
          ),
          buildTextField('Confirmar Senha', ocultarConfSenha,
              TextInputType.text, confSenhaController, (text) {
            if (text.isEmpty) {
              return "Confirmação é obrigatório!";
            } else if (text != senhaController.text) {
              return "Confirmação está diferente da senha.";
            }
            return null;
          }, () {
            modoSenha('confirmar');
          }),
          SizedBox(
            height: 16.0,
          ),
          buildButton("Cadastrar", () {
            if (_formKey.currentState.validate()) {
              cadastrar();
            }
          }),
        ],
      ),
    );
  }

  //Verifica status de loading para exibir a tela de carregamento ou o conteúdo da página
  Widget conteudo() {
    if (loading) {
      return loadingView();
    } else {
      return page();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Criar Conta'),
          centerTitle: true,
        ),
        body: conteudo());
  }
}
