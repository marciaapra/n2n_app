import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:n2n_app/components/BuildAlert.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/components/BuildLoading.dart';
import 'package:n2n_app/components/BuiltDateField.dart';
import 'package:n2n_app/components/BuiltDropDown.dart';
import 'package:n2n_app/components/BuiltTextField.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/models/pessoa.dart';
import 'package:n2n_app/screens/cadDependente.dart';
import 'package:n2n_app/screens/cadTelefone.dart';
import 'package:n2n_app/services/pessoaService.dart';

class EditarTab extends StatefulWidget {
  @override
  _EditarTabState createState() => _EditarTabState();
}

class _EditarTabState extends State<EditarTab> {
  Pessoa pessoa = new Pessoa();

  final _formKey = GlobalKey<FormState>();

  bool loading = true;

  @override
  void initState() {
    super.initState();
    consultar();
  }

  //Consultar dados
  consultar() {
    consultarPessoa().then((response) async {
      pessoa = Pessoa.fromJson(response["objeto"]["dados"]);
      setState(() {
        loading = false;
      });
    }).catchError((error) {
      print('entrou no erro');
      print(error);
    });
  }

  //Editar dados
  editar() {
    //Seta variavel de loading true
    setState(() {
      loading = true;
    });

    var dados = pessoa.toDynamic();
    var dadosAlterar = removerDadosNulos(dados);
    var dadosExcluir = apenasNulos(dados);

    //Chama método editar pessoa
    editarPessoa(dadosAlterar, dadosExcluir).then((response) async {
      //Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Exibe alerta de erro
      return showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return buildAlert(
                Text('SUCESSO'), tratarMsgErro(response['mensagem']), context);
          });
    }).catchError((error) {
      // Seta variavel de loading false
      setState(() {
        loading = false;
      });

      //Exibe alerta de erro
      return showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return buildAlert(
                Text('ATENÇÃO'), tratarMsgErro(error.toString()), context);
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildBodyBack() => Form(
          key: _formKey,
          autovalidate: true,
          child: ListView(
            padding: EdgeInsets.all(12.0),
            children: <Widget>[
              ExpansionTile(
                leading: Icon(Icons.vpn_key),
                title: Text('DADOS DE ACESSO'),
                children: <Widget>[
                  buildTextField2({
                    'label': 'E-mail',
                    'secret': false,
                    'type': TextInputType.emailAddress,
                    'onSaved': (text) => pessoa.email = text,
                    'initialValue': pessoa.email,
                    'validate': (text) {
                      if (text.isEmpty) {
                        return "E-mail é obrigatório!";
                      } else if (!text.contains("@")) {
                        return "E-mail é inválido!";
                      }
                      return null;
                    }
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
              ExpansionTile(
                leading: Icon(Icons.assignment_ind),
                title: Text('DADOS PESSOAIS'),
                children: <Widget>[
                  buildTextField2({
                    'label': 'Nome Completo',
                    'secret': false,
                    'type': TextInputType.text,
                    'onSaved': (text) => pessoa.nomeCompleto = text,
                    'initialValue': pessoa.nomeCompleto,
                    'validate': (text) {
                      if (text.isEmpty) {
                        return "Nome Completo é obrigatório!";
                      }
                      return null;
                    }
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField2({
                    'label': 'Apelido',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.apelido,
                    'onSaved': (text) => pessoa.apelido = text,
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField2({
                    'label': 'CPF',
                    'secret': false,
                    'type': TextInputType.number,
                    'initialValue': pessoa.cpf,
                    'onSaved': (text) => pessoa.cpf = text,
                    'validate': (text) {
                      if (text.isEmpty) {
                        return "CPF é obrigatório!";
                      }
                      return null;
                    }
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField2({
                    'label': 'RG (Opcional)',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.rg.numero,
                    'onSaved': (text) => pessoa.rg.numero = text,
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField2({
                    'label': 'Emissor RG (Opcional)',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.rg.emissor,
                    'onSaved': (text) => pessoa.rg.emissor = text,
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildDateField(
                      true,
                      DateFormat('dd/MM/yyyy'),
                      true,
                      'Data de Nascimento',
                      (value) => setState(() => pessoa.dataNasc = value),
                      pessoa.dataNasc, (text) {
                    if (text == null) {
                      return "Data de Nascimento é obrigatório!";
                    }
                    return null;
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildDropDown('Gênero', optionsGeneros(), pessoa.genero,
                      (value) => setState(() => pessoa.genero = value), (text) {
                    if (text == null) {
                      return "Gênero é obrigatório!";
                    }
                    return null;
                  }),
                  buildTextField2({
                    'label': 'Nacionalidade',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.nacionalidade,
                    'onSaved': (text) => pessoa.nacionalidade = text,
                  }),
                ],
              ),
              ExpansionTile(
                  leading: Icon(Icons.public),
                  title: Text('NATURALIDADE'),
                  children: <Widget>[
                    buildTextField2({
                      'label': 'País',
                      'secret': false,
                      'type': TextInputType.text,
                      'initialValue': pessoa.naturalidade.pais,
                      'onSaved': (text) => pessoa.naturalidade.pais = text,
                    }),
                    SizedBox(
                      height: 16.0,
                    ),
                    buildTextField2({
                      'label': 'UF',
                      'secret': false,
                      'type': TextInputType.text,
                      'initialValue': pessoa.naturalidade.uf,
                      'onSaved': (text) => pessoa.naturalidade.uf = text
                    }),
                    SizedBox(
                      height: 16.0,
                    ),
                    buildTextField2({
                      'label': 'Cidade',
                      'secret': false,
                      'type': TextInputType.text,
                      'initialValue': pessoa.naturalidade.cidade,
                      'onSaved': (text) => pessoa.naturalidade.cidade = text
                    }),
                  ]),
              ExpansionTile(
                leading: Icon(Icons.place),
                title: Text('ENDEREÇO'),
                children: <Widget>[
                  buildTextField2({
                    'label': 'CEP',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.cep,
                    'onSaved': (text) => pessoa.endereco.cep = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'Logradouro',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.logradouro,
                    'onSaved': (text) => pessoa.endereco.logradouro = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'Bairro',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.bairro,
                    'onSaved': (text) => pessoa.endereco.bairro = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'Número',
                    'secret': false,
                    'type': TextInputType.number,
                    'initialValue': pessoa.endereco.numero,
                    'onSaved': (text) => pessoa.endereco.numero = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'Cidade',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.cidade,
                    'onSaved': (text) => pessoa.endereco.cidade = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'UF',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.uf,
                    'onSaved': (text) => pessoa.endereco.uf = text
                  }),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField2({
                    'label': 'Complemento',
                    'secret': false,
                    'type': TextInputType.text,
                    'initialValue': pessoa.endereco.complemento,
                    'onSaved': (text) => pessoa.endereco.complemento = text
                  }),
                ],
              ),
              ExpansionTile(
                  leading: Icon(Icons.call),
                  title: Text('TELEFONES'),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(18.0, 0, 5.0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("(11) 99999-9999"),
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => CadTelefone()));
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    RaisedButton(
                      child: Icon(Icons.add),
                      textColor: Colors.white,
                      color: primaryColor,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => CadTelefone()));
                      },
                    ),
                  ]),
              ExpansionTile(
                  leading: Icon(Icons.people),
                  title: Text('DEPENDENTES'),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(18.0, 0, 5.0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("ANA RIOS"),
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => CadDependente()));
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    RaisedButton(
                      child: Icon(Icons.add),
                      textColor: Colors.white,
                      color: primaryColor,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => CadDependente()));
                      },
                    ),
                  ]),
              // buildButton("Salvar Alteração", editar()),
              buildButton("Salvar Alteração", () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  editar();
                  // return showDialog<void>(
                  //     context: context,
                  //     builder: (BuildContext context) {
                  //       return buildAlertField(
                  //           Row(
                  //             children: <Widget>[
                  //               Expanded(
                  //                 child: buildTextField('Senha', true,
                  //                     TextInputType.text, senhaController),
                  //               ),
                  //             ],
                  //           ),
                  //           context);
                  //     });
                }
              }),
            ],
          ),
        );

    //Verifica status de loading para exibir a tela de carregamento ou o conteúdo da página
    Widget conteudo() {
      if (loading) {
        return loadingView();
      } else {
        return _buildBodyBack();
      }
    }

    return Stack(
      children: <Widget>[conteudo()],
    );
  }
}
