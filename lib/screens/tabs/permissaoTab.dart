import 'package:flutter/material.dart';
import 'package:n2n_app/components/BuildLoading.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/models/projeto.dart';
import 'package:n2n_app/screens/detalhePermissao.dart';
import 'package:n2n_app/services/projetoService.dart';

class PermissaoTab extends StatefulWidget {
  @override
  _PermissaoTabState createState() => _PermissaoTabState();
}

class _PermissaoTabState extends State<PermissaoTab> {
  TextEditingController filterController = TextEditingController();
  String filter;
  List<Projeto> projetos = []; //Lista de todos os projetos
  List<Projeto> listaProjeto = new List<Projeto>(); //Lista de exibição
  bool loading = true;

  //Recarrega página ao receber valores da tela de detalhes
  _getRequests() async {
    setState(() {
      loading = true;
    });
    listaProjeto.clear();
    projetos = [];
    consultar();
  }

  void initState() {
    super.initState();
    consultar();
  }

  //Consultar dados pessoa projeto
  consultar() {
    consultarPessoaProjeto().then((response) async {
      //Popula lista de projetos
      for (dynamic proj in response["objeto"]["dados"]) {
        listaProjeto.add(Projeto.fromJson(proj));
      }
      projetos.addAll(listaProjeto);

      setState(() {
        loading = false;
      });
    }).catchError((error) {
      setState(() {
        loading = false;
      });
    });
  }

  //Busca projetos listados
  void buscar(String texto) {
    List<Projeto> filtroProjeto = List<Projeto>();
    filtroProjeto.addAll(projetos);
    if (texto.isNotEmpty) {
      List<Projeto> dadosLista = List<Projeto>();
      filtroProjeto.forEach((item) {
        if (item.nome.contains(texto.toUpperCase())) {
          dadosLista.add(item);
        }
      });
      setState(() {
        listaProjeto.clear();
        listaProjeto.addAll(dadosLista);
      });
      return;
    } else {
      setState(() {
        listaProjeto.clear();
        listaProjeto.addAll(projetos);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildBodyBack() => Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  this.buscar(value);
                },
                controller: filterController,
                decoration: InputDecoration(
                  hintText: 'Buscar Parceiro',
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ),
            Expanded(
                child: ListView.builder(
              itemCount: listaProjeto.length,
              itemBuilder: (context, index) {
                if (listaProjeto.length > 0) {
                  return Card(
                      elevation: 8.0,
                      margin:
                          EdgeInsets.symmetric(horizontal: 15.0, vertical: 6.0),
                      child: Container(
                          decoration: BoxDecoration(color: Colors.white),
                          child: ListTile(
                            title: Text(
                              '${listaProjeto[index].nome}',
                              style: TextStyle(
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold),
                            ),
                            subtitle: (listaProjeto[index].aprovado
                                ? Text('Acesso aos dados autorizado',
                                    style: TextStyle(color: Colors.green))
                                : Text('Acesso aos dados não autorizado',
                                    style: TextStyle(color: Colors.red))),
                            trailing: Icon(Icons.keyboard_arrow_right,
                                color: primaryColor, size: 30.0),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetalhePermissao(
                                          projeto: listaProjeto[index]))).then(
                                  (val) => val ? _getRequests() : null);
                            },
                          )));
                } else {
                  return Text("Nenhum parceiro encontrado.");
                }
              },
            ))
          ],
        );

    //Verifica status de loading para exibir a tela de carregamento ou o conteúdo da página
    Widget conteudo() {
      if (loading) {
        return loadingView();
      } else {
        return _buildBodyBack();
      }
    }

    return Stack(
      children: <Widget>[conteudo()],
    );
  }
}
