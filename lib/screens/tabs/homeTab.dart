import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/screens/dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  String nome = '';

  @override
  void initState() {
    super.initState();
    getNomeUsuario().then((res) {
      print(nome);
    });
  }

  getNomeUsuario() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nome = (prefs.getString('nome') ?? '');
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildBodyBack() => ListView(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(35),
                      child: Text(
                        'Olá, $nome 😃',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Chip(
                        backgroundColor: primaryColor,
                        label: Text(
                          'Você ainda não possui notificações!',
                          style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                  ]
                ) 
              ),
              Dashboard(1, 'images/parceiros.png', 'Parceiros Associados', '12 parceiros acessam seus dados.'),
              Dashboard(2, 'images/logins.png', 'Último Login', 'Seu último login foi 12/10/2019 em Netflix.'),
              Dashboard(3, 'images/consultas.png', 'Consultas', 'Suas informações foram consultadas 56 vezes.'),
            ],
          //     )
          //],
          // ),
        );

    return Stack(
      children: <Widget>[_buildBodyBack()],
    );
  }
}
