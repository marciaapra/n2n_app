import 'package:flutter/material.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfiguracaoTab extends StatefulWidget {
  @override
  _ConfiguracaoTabState createState() => _ConfiguracaoTabState();
}

class _ConfiguracaoTabState extends State<ConfiguracaoTab> {
  bool notificacao = true;
  bool email = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SwitchListTile(
            title: Text('Notificações do Aplicativo'),
            value: notificacao,
            onChanged: (bool value) {
              setState(() {
                notificacao = value;
              });
            },
            secondary: Icon(Icons.notifications),
          ),
          SwitchListTile(
            title: Text('Envio de E-mail'),
            value: email,
            onChanged: (bool value) {
              setState(() {
                email = value;
              });
            },
            secondary: Icon(Icons.email),
          ),
          SizedBox(
            height: 50.0,
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: RaisedButton(
              child: Text(
                'Excluir Conta',
                style: TextStyle(fontSize: 18.0, color: secondaryColor),
              ),
              textColor: secondaryColor,
              color: Colors.white,
              padding: EdgeInsets.all(10.0),
              onPressed: () {
                final snackBar = SnackBar(content: Text('Em Breve!'));
                Scaffold.of(context).showSnackBar(snackBar);
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: buildButton("Sair", () async {
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('segredo');
              prefs.remove('nome');

              //Redireciona para Login
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login', (Route<dynamic> route) => false);
            }),
          ),
          SizedBox(
            height: 100.0,
            child: Align(
              alignment: Alignment.center,
              child: FlatButton(
                child: Text(
                  "Termo de Uso e Política de Privacidade",
                  style: TextStyle(color: Colors.grey),
                ),
                onPressed: () {
                  //Redirecionar para PDF
                },
              ),
            ),
          ),
          SizedBox(
            height: 50.0,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Versão 1.0.0",
                style: TextStyle(color: Colors.grey),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
