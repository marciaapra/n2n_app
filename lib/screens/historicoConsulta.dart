import 'package:flutter/material.dart';
import 'package:n2n_app/models/projeto.dart';
import 'package:n2n_app/screens/home.dart';

class HistoricoConsulta extends StatefulWidget {
  final Projeto projeto;
  HistoricoConsulta({Key key, this.projeto}) : super(key: key);

  @override
  _HistoricoConsultaState createState() => _HistoricoConsultaState();
}

class _HistoricoConsultaState extends State<HistoricoConsulta> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar(
          'Histórico de Consultas',
        ),
        body: ListView(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            children: <Widget>[
              Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Card(
                        elevation: 8.0,
                        child: Column(
                          children: <Widget>[
                            ExpansionTile(
                                title: Text('12/10/2019 - Requisição de Dados'),
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(
                                          'Necessário para montagem de contratos e para identificar maior de idade.'))
                                ]),
                            ExpansionTile(
                                title: Text('12/10/2019 - Login Externo'),
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(
                                          'Solicitação de credenciais para acesso na plataforma externa.'))
                                ]),
                            ExpansionTile(
                                title: Text('10/10/2019 - Permitido Acesso'),
                                children: <Widget>[
                                  Container(
                                      padding: EdgeInsets.all(10.0),
                                      child: Text(
                                          'Solicitação de consulta aos seus dados foi aceita.'))
                                ])
                          ],
                        ),
                      ),
                    )
                  ])
            ]));
  }
}
