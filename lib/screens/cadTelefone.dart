import 'package:flutter/material.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/components/BuiltDropDown.dart';
import 'package:n2n_app/components/BuiltTextField.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/screens/home.dart';

class CadTelefone extends StatefulWidget {
  @override
  _CadTelefoneState createState() => _CadTelefoneState();
}

class _CadTelefoneState extends State<CadTelefone> {
  final _formKey = GlobalKey<FormState>();

  final ddiController = TextEditingController();
  final dddController = TextEditingController();
  final numeroController = TextEditingController();

  String _tipoTelefone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar(
          'Cadastro de Telefones',
        ),
        body: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16.0),
              children: <Widget>[
                buildDropDown(
                    'Tipo Telefone',
                    optionsTelefones(),
                    _tipoTelefone,
                    (value) => setState(() => _tipoTelefone = value)),
                SizedBox(
                  height: 16.0,
                ),
                buildTextField(
                    'DDI', false, TextInputType.number, ddiController),
                SizedBox(
                  height: 16.0,
                ),
                buildTextField(
                    'DDD', false, TextInputType.number, dddController),
                SizedBox(
                  height: 16.0,
                ),
                buildTextField(
                    'Número', false, TextInputType.number, numeroController),
                SizedBox(
                  height: 16.0,
                ),
                buildButton("Salvar", () {
                  if (_formKey.currentState.validate()) {}
                }),
              ],
            )));
  }
}
