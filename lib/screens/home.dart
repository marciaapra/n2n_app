import 'package:flutter/material.dart';
import 'package:n2n_app/helpers/constantes.dart';
import 'package:n2n_app/screens/tabs/editarTab.dart';
import 'package:n2n_app/screens/tabs/homeTab.dart';
import 'package:n2n_app/screens/tabs/permissaoTab.dart';
import 'package:n2n_app/screens/tabs/configuracaoTab.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _page = 0;
  String titulo = 'Home';
  PageController _pageController;
  CircularBottomNavigationController _navigationController;

  @override
  void initState() {
    super.initState();
    _navigationController = CircularBottomNavigationController(_page);
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _navigationController.dispose();
    super.dispose();
  }

  //Atribui valor ao titulo da tela
  setTitulo() {
    switch (_page) {
      case 0:
        setState(() {
          this.titulo = 'N2N';
        });
        break;
      case 1:
        setState(() {
          this.titulo = 'Edição de Dados';
        });
        break;
      case 2:
        setState(() {
          this.titulo = 'Permissões de Acesso';
        });
        break;
      case 3:
        setState(() {
          this.titulo = 'Configurações';
        });
        break;
      default:
        setState(() {
          this.titulo = 'N2N';
        });
    }
  }

  List<TabItem> tabItems = List.of([
    new TabItem(Icons.home, "Home", secondaryColor),
    new TabItem(Icons.edit, "Editar", secondaryColor),
    new TabItem(Icons.verified_user, "Permissões", secondaryColor),
    new TabItem(Icons.settings, "Configuração", secondaryColor),
  ]);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(this.titulo),
      body: PageView(
        controller: _pageController,
        onPageChanged: (page) {
          setState(() {
            this._page = page;
            _navigationController.value = _page;
            setTitulo();
          });
        },
        children: <Widget>[
          HomeTab(),
          EditarTab(),
          PermissaoTab(),
          ConfiguracaoTab(),
        ],
      ),
      bottomNavigationBar:
          getBottomNavigator(tabItems, _navigationController, _pageController),
    );
  }
}

Widget getBottomNavigator(tabItems, _navigationController, _pageController) {
  return CircularBottomNavigation(
    tabItems,
    controller: _navigationController,
    barBackgroundColor: Colors.white,
    normalIconColor: primaryColor,
    iconsSize: 22.0,
    circleSize: 50.0,
    circleStrokeWidth: 2.0,
    barHeight: 55.0,
    selectedCallback: (page) {
      _pageController.animateToPage(page,
          duration: Duration(microseconds: 500), curve: Curves.ease);
    },
  );
}

Widget getAppBar(title, [actions]) {
  return AppBar(title: Text(title), actions: actions);
}
