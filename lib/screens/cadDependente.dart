import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:n2n_app/components/BuildButton.dart';
import 'package:n2n_app/components/BuiltDateField.dart';
import 'package:n2n_app/components/BuiltDropDown.dart';
import 'package:n2n_app/components/BuiltTextField.dart';
import 'package:n2n_app/helpers/biblioteca.dart';
import 'package:n2n_app/screens/home.dart';

class CadDependente extends StatefulWidget {
  @override
  _CadDependenteState createState() => _CadDependenteState();
}

class _CadDependenteState extends State<CadDependente> {
  final _formKey = GlobalKey<FormState>();

  final cepController = TextEditingController();
  final logradouroController = TextEditingController();
  final bairroController = TextEditingController();
  final numeroController = TextEditingController();
  final cidadeController = TextEditingController();
  final ufController = TextEditingController();
  final complementoController = TextEditingController();
  final paisNatController = TextEditingController();
  final ufNatController = TextEditingController();
  final cidadeNatController = TextEditingController();
  final nomeController = TextEditingController();
  final cpfController = TextEditingController();
  final rgController = TextEditingController();
  final emiRgController = TextEditingController();
  final nacionalidadeController = TextEditingController();
  final emailController = TextEditingController();
  final senhaController = TextEditingController();

  DateTime _date;
  String _genero;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar(
          'Cadastro de Dependentes',
        ),
        body: Form(
            key: _formKey,
            child: ListView(padding: EdgeInsets.all(12.0), children: <Widget>[
              ExpansionTile(
                leading: Icon(Icons.vpn_key),
                title: Text('DADOS DE ACESSO'),
                children: <Widget>[
                  buildTextField('E-mail', false, TextInputType.emailAddress,
                      emailController, (text) {
                    if (text.isEmpty) {
                      return "E-mail é obrigatório!";
                    } else if (!text.contains("@")) {
                      return "E-mail é inválido!";
                    }
                    return null;
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField(
                    'Senha',
                    true,
                    TextInputType.text,
                    senhaController,
                    (text) {
                      if (text.isEmpty) {
                        return "Senha é obrigatório!";
                      }
                      return null;
                    },
                  ),
                ],
              ),
              ExpansionTile(
                leading: Icon(Icons.assignment_ind),
                title: Text('DADOS PESSOAIS'),
                children: <Widget>[
                  buildTextField('Nome Completo', false, TextInputType.text,
                      nomeController, (text) {
                    if (text.isEmpty) {
                      return "Nome Completo é obrigatório!";
                    }
                    return null;
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField(
                    'CPF',
                    false,
                    TextInputType.number,
                    cpfController,
                    (text) {
                      if (text.isEmpty) {
                        return "CPF é obrigatório!";
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField(
                      'RG (Opcional)', false, TextInputType.text, rgController),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildTextField('Emissor RG (Opcional)', false,
                      TextInputType.text, emiRgController),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildDateField(
                      true,
                      DateFormat('dd/MM/yyyy'),
                      true,
                      'Data de Nascimento',
                      (value) => setState(() => _date = value),
                      null, (text) {
                    if (text == null) {
                      return "Data de Nascimento é obrigatório!";
                    }
                    return null;
                  }),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildDropDown('Gênero', optionsGeneros(), _genero,
                      (value) => setState(() => _genero = value), (text) {
                    if (text == null) {
                      return "Gênero é obrigatório!";
                    }
                    return null;
                  }),
                  buildTextField('Nacionalidade', false, TextInputType.text,
                      nacionalidadeController),
                ],
              ),
              ExpansionTile(
                  leading: Icon(Icons.public),
                  title: Text('NATURALIDADE'),
                  children: <Widget>[
                    buildTextField(
                        'País', false, TextInputType.text, paisNatController),
                    SizedBox(
                      height: 16.0,
                    ),
                    buildTextField(
                        'UF', false, TextInputType.text, ufNatController),
                    SizedBox(
                      height: 16.0,
                    ),
                    buildTextField('Cidade', false, TextInputType.text,
                        cidadeNatController),
                  ]),
              ExpansionTile(
                leading: Icon(Icons.place),
                title: Text('ENDEREÇO'),
                children: <Widget>[
                  buildTextField(
                      'CEP', false, TextInputType.text, cepController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField('Logradouro', false, TextInputType.text,
                      logradouroController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField(
                      'Bairro', false, TextInputType.text, bairroController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField(
                      'Número', false, TextInputType.number, numeroController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField(
                      'Cidade', false, TextInputType.text, cidadeController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField('UF', false, TextInputType.text, ufController),
                  SizedBox(
                    height: 16.0,
                  ),
                  buildTextField('Complemento', false, TextInputType.text,
                      complementoController),
                ],
              ),
              buildButton("Salvar", () {
                if (_formKey.currentState.validate()) {}
              }),
            ])));
  }
}
