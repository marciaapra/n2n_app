class Endereco {
  String cep;
  String logradouro;
  String numero;
  String bairro;
  String cidade;
  String uf;
  String complemento;

  Endereco(
      {this.cep,
      this.logradouro,
      this.numero,
      this.bairro,
      this.cidade,
      this.uf,
      this.complemento});

  factory Endereco.fromJson(Map<String, dynamic> json) {
    var cep = json != null ? json['cep'] : null;
    var logradouro = json != null ? json['logradouro'] : null;
    var numero = json != null ? json['numero'] : null;
    var bairro = json != null ? json['bairro'] : null;
    var cidade = json != null ? json['cidade'] : null;
    var uf = json != null ? json['uf'] : null;
    var complemento = json != null ? json['complemento'] : null;

    return Endereco(
        cep: cep,
        logradouro: logradouro,
        numero: numero,
        bairro: bairro,
        cidade: cidade,
        uf: uf,
        complemento: complemento);
  }

  dynamic toDynamic() {
    var dados = {
      'cep': this.cep,
      'logradouro': this.logradouro,
      'bairro': this.bairro,
      'numero': this.numero,
      'cidade': this.cidade,
      'uf': this.uf,
      'complemento': this.complemento
    };

    return dados;
  }
}
