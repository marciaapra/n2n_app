import 'package:n2n_app/models/endereco.dart';
import 'package:n2n_app/models/naturalidade.dart';
import 'package:n2n_app/models/rg.dart';

class Pessoa {
  int id;
  String nomeCompleto;
  String apelido;
  String cpf;
  Rg rg;
  DateTime dataNasc;
  String genero;
  String email;
  String senha;
  String nacionalidade;
  Naturalidade naturalidade;
  Endereco endereco;

  Pessoa(
      {this.id,
      this.nomeCompleto,
      this.apelido,
      this.cpf,
      this.dataNasc,
      this.genero,
      this.rg,
      this.email,
      this.senha,
      this.nacionalidade,
      this.naturalidade,
      this.endereco});

  factory Pessoa.fromJson(Map<String, dynamic> json) {
    return Pessoa(
        id: json['id'],
        nomeCompleto: json['nome_completo'],
        apelido: json['apelido'],
        cpf: json['cpf'],
        dataNasc: DateTime.parse(json['data_nasc']),
        genero: json['genero'],
        rg: Rg.fromJson(json['rg']),
        email: json['email'],
        senha: json['senha'],
        nacionalidade: json['nacionalidade'],
        naturalidade: Naturalidade.fromJson(json['naturalidade']),
        endereco: Endereco.fromJson(json['endereco']));
  }

  dynamic toDynamic() {
    var dados = {
      'email': this.email,
      'nome_completo': this.nomeCompleto,
      'apelido': this.apelido,
      'cpf': this.cpf.toString(),
      'data_nasc': this.dataNasc.toString(),
      'genero': this.genero,
      'rg': this.rg.toDynamic(),
      'endereco': this.endereco.toDynamic(),
      'naturalidade': this.naturalidade.toDynamic(),
      'nacionalidade': this.nacionalidade
    };

    return dados;
  }
}
