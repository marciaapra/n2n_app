import 'package:n2n_app/models/requerimento.dart';

class Projeto {
  String id;
  String idEmpresa;
  String nome;
  bool aprovado;
  DateTime dataAprovacao;
  DateTime dataAtualizacao;
  List<Requerimento> requerimentos;

  Projeto(
      {this.id,
      this.idEmpresa,
      this.nome,
      this.aprovado,
      this.dataAprovacao,
      this.dataAtualizacao,
      this.requerimentos});

  factory Projeto.fromJson(Map<String, dynamic> json) {
    //Trata lista de requerimentos
    var list = json['requerimentos'] as List;
    List<Requerimento> requerimentoList =
        list.map((i) => Requerimento.fromJson(i)).toList();

    return Projeto(
        id: json['_id'],
        idEmpresa: json['empresa_id'],
        nome: json['nome_projeto'].toUpperCase(),
        aprovado: json['status'],
        dataAprovacao: DateTime.parse(json['criacao_vinculo']),
        dataAtualizacao: DateTime.parse(json['ultimo_login']),
        requerimentos: requerimentoList);
  }
}
