class Naturalidade {
  String pais;
  String uf;
  String cidade;

  Naturalidade({this.pais, this.uf, this.cidade});

  factory Naturalidade.fromJson(Map<String, dynamic> json) {
    var pais = json != null ? json['pais'] : null;
    var uf = json != null ? json['uf'] : null;
    var cidade = json != null ? json['cidade'] : null;

    return Naturalidade(pais: pais, uf: uf, cidade: cidade);
  }

  dynamic toDynamic() {
    var dados = {'cidade': this.cidade, 'uf': this.uf, 'pais': this.pais};

    return dados;
  }
}
