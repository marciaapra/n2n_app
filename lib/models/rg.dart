class Rg {
  String emissor;
  String numero;

  Rg({this.emissor, this.numero});

  factory Rg.fromJson(Map<String, dynamic> json) {
    var emissor = json != null ? json['emissor'] : null;
    var numero = json != null ? json['numero'] : null;

    return Rg(
      emissor: emissor,
      numero: numero,
    );
  }

  dynamic toDynamic() {
    var dados = {'numero': this.numero, 'emissor': this.emissor};

    return dados;
  }
}
