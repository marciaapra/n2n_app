class Requerimento {
  String campo;
  String motivo;

  Requerimento({this.campo, this.motivo});

  factory Requerimento.fromJson(Map<String, dynamic> json) {
    var campo = json != null ? json['campo'] : null;
    var motivo = json != null ? json['motivo'] : null;

    return Requerimento(
      campo: campo,
      motivo: motivo,
    );
  }
}
