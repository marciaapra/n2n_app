import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:n2n_app/helpers/constantes.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

//Método cadastrar pessoa
Future<Map> cadastrarPessoa(dados) async {
  final String url = apiUrl + 'pessoa/cadastro';
  final headers = {'Content-Type': 'application/json'};

  print(dados);
  //Envia requisição de cadastro
  return http
      .post(url, headers: headers, body: json.encode(dados))
      .then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}

//Método consultar pessoa
Future<Map> consultarPessoa() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String segredo = prefs.getString("segredo");

  final String url = apiUrl + 'pessoa/consultar/' + segredo;
  final headers = {'Content-Type': 'application/json'};
  print(url);
  //Envia requisição de cadastro
  return http.get(url, headers: headers).then((response) async {
    Map retorno = json.decode(response.body);

    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}

//Método editar pessoa
Future<Map> editarPessoa(dadosAlterar, dadosExcluir) async {
  final String url = apiUrl + 'pessoa/editar_dados';
  final headers = {'Content-Type': 'application/json'};

  //Captura id do usuário logado
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String segredo = prefs.getString("segredo");

  //Prepara dados para envio
  var dadosEnv = {
    '_id': segredo,
    'dados_editados': dadosAlterar,
    'dados_excluidos': dadosExcluir
  };

  print("AQUI");
  print(dadosEnv);

  //Envia requisição de edição
  return http
      .put(url, headers: headers, body: json.encode(dadosEnv))
      .then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}

//Método excluir dados/campos da pessoa
Future<Map> excluirCamposPessoa(dados) async {
  final String url = apiUrl + 'pessoa/excluir_dados';
  final headers = {'Content-Type': 'application/json'};

  //Captura id do usuário logado
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String segredo = prefs.getString("segredo");

  //Prepara dados para envio
  var dadosEnv = {'_id': segredo, 'dados_excluidos': dados};

  //Envia requisição de edição
  return http
      .put(url, headers: headers, body: json.encode(dadosEnv))
      .then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}
