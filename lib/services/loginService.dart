import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:n2n_app/helpers/constantes.dart';
import 'dart:async';

//Método logar pessoa
Future<Map> logar(dados) async {
  final String url = apiUrl + 'pessoa/login';
  final headers = {'Content-Type': 'application/json'};

  //Envia requisição de cadastro
  return http
      .post(url, headers: headers, body: json.encode(dados))
      .then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}
