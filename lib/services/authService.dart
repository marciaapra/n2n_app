import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  // Verifica se está Logado
  Future<bool> isLogado() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String segredo = prefs.getString("segredo");
    return segredo != null ? true : false;
  }
}