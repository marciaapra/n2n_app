import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:n2n_app/helpers/constantes.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

//Método consultar os projetos da pessoa logada
Future<Map> consultarPessoaProjeto() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String segredo = prefs.getString("segredo");

  final String url = apiUrl + 'projeto/consulta_por_pessoa/' + segredo;
  final headers = {'Content-Type': 'application/json'};

  //Envia requisição de cadastro
  return http.get(url, headers: headers).then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}

//Método para enviar a resposta de aceite/recusa dos termos do projeto
Future<Map> aceitarTermos(segredo, aceito) async {
  final String url = apiUrl + 'projeto/aceite_termos';
  final headers = {'Content-Type': 'application/json'};

  //Prepara dados para envio
  var dados = {'segredo': segredo, 'aceito': aceito};

  print("AQUI");
  print(dados);

  //Envia requisição de edição
  return http
      .put(url, headers: headers, body: json.encode(dados))
      .then((response) async {
    Map retorno = json.decode(response.body);
    print(retorno);
    //Verifica se o código retornado é ok
    if (retorno['codigo'] == 200 || retorno['codigo'] == 201) {
      return retorno;
    } else {
      throw new Exception(retorno['mensagem']);
    }
  });
}
